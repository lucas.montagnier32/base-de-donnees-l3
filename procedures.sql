--pas vérifié
--fonction qui calcule_le gain d'un conducteur sur tous ses trajets
CREATE OR REPLACE FUNCTION calcul_gain_conducteur (idc NUMBER)
RETURN INTEGER IS
    existe_pas_error EXCEPTION;
    CURSOR c1 IS SELECT prix_brut, marge
				FROM trajet NATURAL JOIN avis
                WHERE id_cond=idc;
	ligne c1%ROWTYPE;
	gain NUMBER := 0;
	nb_clients NUMBER;
BEGIN
    SELECT count(*) INTO nb_clients FROM trajet NATURAL JOIN avis WHERE id_cond=idc;
    IF nb_clients<=0 THEN
        RAISE existe_pas_error;
    END IF;
    
    OPEN c1;
	LOOP
		FETCH c1 into ligne;
		EXIT WHEN c1%NOTFOUND;
		gain := gain+ligne.prix_brut-ligne.marge;
	END LOOP;
	CLOSE c1;
	RETURN gain;
	
    EXCEPTION
        WHEN existe_pas_error THEN
        RAISE_APPLICATION_ERROR(-20003,'id_cond does not exist or this driver has no clients');
END calcul_gain_conducteur;
/

--SELECT calcul_gain_conducteur(2) from trajet GROUP BY calcul_gain_conducteur(2);
--SELECT calcul_gain_conducteur(20) from trajet GROUP BY calcul_gain_conducteur(20);

--SELECT prix_brut, marge FROM trajet NATURAL JOIN avis where id_cond=1;

--fonction qui calcule la moyenne des notes d'un id_cond pour tous ses trajets (OK)
CREATE OR REPLACE FUNCTION calcul_moyenne_id_cond (id NUMBER)
RETURN REAL IS
    la_moyenne NUMBER;
    nbLignes NUMBER;
    id_error EXCEPTION;
BEGIN
    SELECT count(*) INTO nbLignes FROM trajet WHERE id_cond=id;
    IF nbLignes < 1 THEN 
        RAISE id_error;
    ELSE
        SELECT avg(note) INTO la_moyenne FROM trajet NATURAL JOIN avis WHERE trajet.id_cond=id;
        RETURN la_moyenne;
    END IF;
    EXCEPTION
        WHEN id_error THEN
        RAISE_APPLICATION_ERROR(-20001,'Ce conducteur n existe pas');
END calcul_moyenne_id_cond;
/

--SELECT calcul_moyenne_id_cond(2) FROM trajet GROUP BY calcul_moyenne_id_cond(2);
--SELECT calcul_moyenne_id_cond(3) FROM trajet GROUP BY calcul_moyenne_id_cond(3);

--procedure creeVoiture avec les 3 paramètres du style suivant (OK)
CREATE OR REPLACE PROCEDURE creer_voiture (modele VARCHAR2, couleur VARCHAR2, nbplaces NUMBER) IS
BEGIN
    INSERT INTO voiture VALUES (seq_novoiture.NEXTVAL, modele, couleur, nbplaces);
    COMMIT;
END;
/
--EXECUTE creer_voiture ('Subaru','Rose',5);

GRANT execute on calcul_gain_conducteur TO L3_32_37_conducteur;
GRANT execute on calcul_moyenne_id_cond TO L3_32_37_conducteur;
GRANT execute on creer_voiture TO L3_32_37_conducteur;
