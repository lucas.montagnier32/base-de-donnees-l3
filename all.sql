
set linesize 200;

prompt *************************************************************
prompt ********************* DROP INDEX *****************************
prompt *************************************************************

DROP INDEX idx_prix_brut;

prompt *************************************************************
prompt ******************** DROP TABLE *****************************
prompt *************************************************************

DROP TABLE TRAJET CASCADE CONSTRAINTS;
DROP TABLE CONDUCTEUR CASCADE CONSTRAINTS;
DROP TABLE CLIENT CASCADE CONSTRAINTS;
DROP TABLE AVIS CASCADE CONSTRAINTS;
DROP TABLE VOITURE CASCADE CONSTRAINTS;

prompt *************************************************************
prompt ******************** CREATE TABLE ***************************
prompt *************************************************************

-- Nous n'avons pas les privilÃ¨ges pour crÃ©er un cluster
--CREATE CLUSTER avis_trajet (id_trajet NUMBER)
    --SIZE 600
    --TABLESPACE users
    --STORAGE (INITIAL 100K NEXT 50K);
    	--CLUSTER avis_trajet(id_trajet);
   	--CLUSTER avis_trajet(id_trajet);

CREATE TABLE VOITURE (
	id_voiture NUMBER,
	modele VARCHAR2(40),
	couleur VARCHAR(20),
	nbplaces NUMBER,
	CONSTRAINT pk_voiture PRIMARY KEY(id_voiture),
	CONSTRAINT check_nbPlaces CHECK (nbplaces BETWEEN 1 AND 7)
);

CREATE TABLE CONDUCTEUR (
	id_cond NUMBER,
	id_voiture NUMBER,
	nom_cond	VARCHAR2(40),
	prenom_cond	VARCHAR2(40),
	CONSTRAINT pk_conducteur PRIMARY KEY(id_cond),
	CONSTRAINT fk_conducteur_voiture FOREIGN KEY(id_voiture) REFERENCES VOITURE(id_voiture)
);

CREATE TABLE TRAJET (
	id_trajet NUMBER,
	id_cond NUMBER,
	lieu_dep	VARCHAR2(40),
	lieu_arr	VARCHAR2(40),
	date_dep	TIMESTAMP,
	date_arr	TIMESTAMP,
	prix_brut NUMBER,
	marge NUMBER,
	CONSTRAINT pk_trajet PRIMARY KEY(id_trajet),
	CONSTRAINT fk_trajet_conducteur FOREIGN KEY(id_cond) REFERENCES CONDUCTEUR(id_cond),
	CONSTRAINT check_date_dep_arr CHECK (date_dep < date_arr),
	CONSTRAINT check_prix_marge CHECK (marge < prix_brut)
);

CREATE TABLE CLIENT (
	id_cli NUMBER,
	nom_cli VARCHAR2(40),
	prenom_cli VARCHAR2(40),
	CONSTRAINT pk_client PRIMARY KEY(id_cli)
);

CREATE TABLE AVIS (
	avis VARCHAR2(100),
	note NUMBER,
	id_cli NUMBER,
	id_trajet NUMBER,
	CONSTRAINT pk_avis PRIMARY KEY(id_trajet,id_cli),
	CONSTRAINT fk_trajet_avis_idci FOREIGN KEY(id_cli) REFERENCES CLIENT(id_cli),
	CONSTRAINT fk_trajet_avis_idtraj FOREIGN KEY(id_trajet) REFERENCES TRAJET(id_trajet),
	CONSTRAINT check_note CHECK (note BETWEEN 0 AND 5)
);

prompt *************************************************************
prompt ********************* DROP ROLE *****************************
prompt *************************************************************

DROP ROLE L3_35_37_client;
DROP ROLE L3_32_37_conducteur;
DROP ROLE L3_44_admin;

prompt *************************************************************
prompt ******************** CREATE ROLE ****************************
prompt *************************************************************

CREATE ROLE L3_35_37_client;
CREATE ROLE L3_32_37_conducteur;
CREATE ROLE L3_44_admin;

GRANT L3_44_admin TO L3_44;
GRANT L3_35_37_client, L3_32_37_conducteur TO L3_37;
GRANT L3_35_37_client TO L3_35;
GRANT L3_32_37_conducteur TO L3_32;

GRANT ALL ON CONDUCTEUR TO L3_44_admin;
GRANT ALL ON TRAJET TO L3_44_admin;
GRANT ALL ON CLIENT TO L3_44_admin;
GRANT ALL ON AVIS TO L3_44_admin;
GRANT ALL ON VOITURE TO L3_44_admin;

GRANT select, update, insert, delete on CLIENT TO L3_35_37_client;
GRANT select, update, insert, delete on AVIS TO L3_35_37_client;
GRANT select on CONDUCTEUR TO L3_35_37_client;
GRANT select on VOITURE TO L3_35_37_client;
GRANT select on TRAJET TO L3_35_37_client;

GRANT select, update, insert, delete on TRAJET TO L3_32_37_conducteur;
GRANT select, update, insert, delete on CONDUCTEUR TO L3_32_37_conducteur;
GRANT select, update, insert on VOITURE TO L3_32_37_conducteur;
GRANT select on AVIS TO L3_32_37_conducteur;
GRANT select on CLIENT TO L3_32_37_conducteur;


prompt *************************************************************
prompt ******************** CREATE INDEX ****************************
prompt *************************************************************

CREATE INDEX idx_prix_brut ON trajet (prix_brut);


prompt *************************************************************
prompt ********************* DROP VIEW *****************************
prompt *************************************************************

DROP VIEW trajet_une_personne;
DROP VIEW trajet_par_conducteur;

prompt *************************************************************
prompt ******************** CREATE VIEW ****************************
prompt *************************************************************

CREATE OR REPLACE VIEW trajet_une_personne AS
    SELECT *
    FROM trajet NATURAL JOIN avis;
    
CREATE OR REPLACE VIEW trajet_par_conducteur AS
    SELECT *
    FROM trajet NATURAL JOIN conducteur;



prompt *************************************************************
prompt ******************* DELETE TABLE ****************************
prompt *************************************************************

DELETE FROM AVIS;
DELETE FROM TRAJET;
DELETE FROM CONDUCTEUR;
DELETE FROM CLIENT;
DELETE FROM VOITURE;

prompt *************************************************************
prompt ********************** INSERT *******************************
prompt *************************************************************

DROP sequence seq_novoiture;
CREATE SEQUENCE seq_novoiture START WITH 50 INCREMENT BY 1;


insert into VOITURE values (1, 'Peugeot', 'Marron', 4);
insert into VOITURE values (2, 'Toyota', 'Grise', 4);
insert into VOITURE values (3, 'Twingo', 'Verte', 3);
insert into VOITURE values (4, 'Ferrari', 'Rouge', 3);
insert into VOITURE values (5, 'Peugeot', 'Marron', 3);
insert into VOITURE values (6, 'Renault', 'Blanche', 4);
insert into VOITURE values (7, 'Citroen', 'Blanche', 7);

insert into CONDUCTEUR values (1, 1, 'Tanguay','Arnaud');
insert into CONDUCTEUR values (2, 2, 'Fondemie','Chappell');
insert into CONDUCTEUR values (4, 3, 'Barrette','Ancelina');
insert into CONDUCTEUR values (6, 4, 'Neufville','Arlette');
insert into CONDUCTEUR values (8, 5, 'Barrette','Jacqueline');
insert into CONDUCTEUR values (10, 6, 'Neufville','Arlette');
insert into CONDUCTEUR values (13, 7, 'Faemlin','Ancelina');

insert into TRAJET values (1, 1, 'Nantes', 'Paris', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 31, 6);
insert into TRAJET values (2, 4, 'Nantes', 'Paris',TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 32, 6);
insert into TRAJET values (3, 2, 'Mulhouse', 'Marseille', TIMESTAMP '2015-03-12 8:37:00', TIMESTAMP '2015-03-12 17:00:00', 50, 10);
insert into TRAJET values (4, 6, 'Latronche', 'Arnac-La-Poste', TIMESTAMP '2018-04-14 05:08:00', TIMESTAMP '2018-04-14 07:30:00', 14, 2);
insert into TRAJET values (5, 8, 'Nantes', 'Tours', TIMESTAMP '2018-04-14 05:08:00', TIMESTAMP '2018-04-14 07:30:00', 14, 2);
insert into TRAJET values (6, 10, 'Arnac-La-Poste', 'Latronche', TIMESTAMP '2020-11-21 08:00:00', TIMESTAMP '2020-11-21 10:30:00', 15, 2);
insert into TRAJET values (7, 2, 'Mouais', 'Latronche', TIMESTAMP '2018-04-04 07:30:00', TIMESTAMP '2018-04-04 13:30:00', 15, 2);
insert into TRAJET values (8, 13, 'Poil', 'Plurien', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 20, 4);

insert into CLIENT values (1, 'Tanguay','Arnaud');
insert into CLIENT values (2, 'Jacob','Ancelina');
insert into CLIENT values (3, 'Bordeaux','Victoire');
insert into CLIENT values (5, 'Girard','Astrid');
insert into CLIENT values (7, 'Bordeaux','Victoire');
insert into CLIENT values (9, 'Soucy','Iven');
insert into CLIENT values (11, 'Brunault','Astrid');
insert into CLIENT values (12, 'Brunault','Ruby');
insert into CLIENT values (14, 'Gamelin','Ruby');

insert into AVIS values ('Arnaud, il est bien!', 4, 2, 1);
insert into AVIS values ('Trajet satisfaisant', 3, 3, 1);
insert into AVIS values (':(', 1, 5, 2);
insert into AVIS values ('J ai bien aime ce trajet avec Angelina', 3, 1, 3);
insert into AVIS values ('Comentaire!!!', 1, 3, 4);
insert into AVIS values ('J adore la voiture', 4, 7, 4);
insert into AVIS values ('C est ici qu il faut mettre un commentaire?', 0, 9, 5);
insert into AVIS values ('Trajet tres agreable en compagnie d Arlette', 4, 11, 6);
insert into AVIS values ('5/5', 4, 12, 7);
insert into AVIS values ('Trajet satisfaisant', 3, 14, 8);


prompt *************************************************************
prompt ************** FUNCTION AND PROCEDURE ***********************
prompt *************************************************************

--fonction qui calcule_le gain d'un conducteur sur tous ses trajets (OK)
CREATE OR REPLACE FUNCTION calcul_gain_conducteur (idc NUMBER)
RETURN INTEGER IS
    existe_pas_error EXCEPTION;
    CURSOR c1 IS SELECT prix_brut, marge
				FROM trajet NATURAL JOIN avis
                WHERE id_cond=idc;
	ligne c1%ROWTYPE;
	gain NUMBER := 0;
	nb_clients NUMBER;
BEGIN
    SELECT count(*) INTO nb_clients FROM trajet NATURAL JOIN avis WHERE id_cond=idc;
    IF nb_clients<=0 THEN
        RAISE existe_pas_error;
    END IF;
    
    OPEN c1;
	LOOP
		FETCH c1 into ligne;
		EXIT WHEN c1%NOTFOUND;
		gain := gain+ligne.prix_brut-ligne.marge;
	END LOOP;
	CLOSE c1;
	RETURN gain;
	
    EXCEPTION
        WHEN existe_pas_error THEN
        RAISE_APPLICATION_ERROR(-20003,'id_cond does not exist or this driver has no clients');
END calcul_gain_conducteur;
/
--TEST
--SELECT calcul_gain_conducteur(2) from trajet GROUP BY calcul_gain_conducteur(2);
--SELECT calcul_gain_conducteur(20) from trajet GROUP BY calcul_gain_conducteur(20);


--fonction qui calcule la moyenne des notes d'un id_cond pour tous ses trajets (OK)
CREATE OR REPLACE FUNCTION calcul_moyenne_id_cond (id NUMBER)
RETURN REAL IS
    la_moyenne NUMBER;
    nbLignes NUMBER;
    id_error EXCEPTION;
BEGIN
    SELECT count(*) INTO nbLignes FROM trajet WHERE id_cond=id;
    IF nbLignes < 1 THEN 
        RAISE id_error;
    ELSE
        SELECT avg(note) INTO la_moyenne FROM trajet NATURAL JOIN avis WHERE trajet.id_cond=id;
        RETURN la_moyenne;
    END IF;
    EXCEPTION
        WHEN id_error THEN
        RAISE_APPLICATION_ERROR(-20001,'Ce conducteur n existe pas');
END calcul_moyenne_id_cond;
/

--SELECT calcul_moyenne_id_cond(2) FROM trajet GROUP BY calcul_moyenne_id_cond(2);
--SELECT calcul_moyenne_id_cond(3) FROM trajet GROUP BY calcul_moyenne_id_cond(3);

--procedure creeVoiture avec les 3 paramètres du style suivant (OK)
CREATE OR REPLACE PROCEDURE creer_voiture (modele VARCHAR2, couleur VARCHAR2, nbplaces NUMBER) IS
BEGIN
    INSERT INTO voiture VALUES (seq_novoiture.NEXTVAL, modele, couleur, nbplaces);
    COMMIT;
END;
/
--TEST
--delete from voiture where modele='Subaru' AND couleur='Rose';
--EXECUTE creer_voiture ('Subaru','Rose',5);
--select * from voiture;

GRANT execute on calcul_gain_conducteur TO L3_32_37_conducteur;
GRANT execute on calcul_moyenne_id_cond TO L3_32_37_conducteur;
GRANT execute on creer_voiture TO L3_32_37_conducteur;
GRANT execute on calcul_gain_conducteur TO L3_44_admin;
GRANT execute on calcul_moyenne_id_cond TO L3_44_admin;
GRANT execute on creer_voiture TO L3_44_admin;


prompt *************************************************************
prompt *********************** TRIGGER *****************************
prompt *************************************************************

--trigger calculant automatiquement la marge a l'ajout d'un trajet (OK)
CREATE OR REPLACE TRIGGER trajet_bef_ins_row_marge
BEFORE INSERT ON trajet
FOR EACH ROW
DECLARE
    percent_marge number;
BEGIN
    percent_marge := 0.2;
    
    :new.marge := (percent_marge*:new.prix_brut);
END;
/
--TEST
--delete from trajet where id_trajet=9;
--insert into TRAJET values (9, 13, 'villetest', 'villetest', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 40, 0);
--select * from trajet;

--verif que le nombre de client dans un trajet est inférieur au nombre de place quand on veut ajouter un client dans un trajet (OK)
CREATE OR REPLACE TRIGGER avis_bef_ins_row_nbplaces
BEFORE INSERT ON avis
FOR EACH ROW
DECLARE
    nb_client_traj NUMBER;
    nb_places NUMBER;
    pas_de_place_error EXCEPTION;
BEGIN
    SELECT count(*) INTO nb_client_traj FROM avis WHERE id_trajet=:new.id_trajet;
    SELECT nbplaces INTO nb_places FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet WHERE id_trajet=:new.id_trajet GROUP BY nbplaces;
    IF nb_client_traj>=nb_places THEN
        RAISE pas_de_place_error;
    END IF;
    EXCEPTION
        WHEN pas_de_place_error THEN
        RAISE_APPLICATION_ERROR(-20002, 'Il ne peut pas y avoir plus d avis');
END;
/

---TEST trigger ci-dessus
---Création d'un client
--insert into client values (5664, 'test', 'test');

---Vérification du nombre d'avis laissé sur une voiture donc du nombre de personne sur ce trajet
--select nbplaces FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet where id_trajet=1 group by nbplaces;

---Vérification du nombre de places dans la voiture
--select count(*) from avis where id_trajet=1;

---Modification du nombre de places de la voiture 1
--update voiture set nbplaces=2 where id_voiture=1;

---nombre de places dispo sur le trajet maintenant
--select nbplaces FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet where id_trajet=1 group by nbplaces;

---Création de l'avis (doit bloquer)
--insert into avis values ('tes', 5, 5664, 1);

---Modification du nombre de places de la voiture 1
--update voiture set nbplaces=3 where id_voiture=1;

---nombre de places dispo sur le trajet maintenant
--select nbplaces FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet where id_trajet=1 group by nbplaces;

---Création de l'avis (ne doit pas bloquer)
--insert into avis values ('tes', 5, 5664, 1)

---Remise a l'état comme avant
--delete from avis where id_cli=5664 and id_trajet=1;
--delete from client where id_cli=5664;

