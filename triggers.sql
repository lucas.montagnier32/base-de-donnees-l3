--trigger calculant automatiquement la marge a l'ajout d'un trajet (OK)
CREATE OR REPLACE TRIGGER trajet_bef_ins_row_marge
BEFORE INSERT ON trajet
FOR EACH ROW
DECLARE
    percent_marge number;
BEGIN
    percent_marge := 0.2;
    
    :new.marge := (percent_marge*:new.prix_brut);
END;
/

--insert into TRAJET values (9, 13, 'villetest', 'villetest', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 40, 0);

--verif que le nombre de client dans un trajet est inférieur au nombre de place quand on veut ajouter un client dans un trajet (OK)
CREATE OR REPLACE TRIGGER avis_bef_ins_row_nbplaces
BEFORE INSERT ON avis
FOR EACH ROW
DECLARE
    nb_client_traj NUMBER;
    nb_places NUMBER;
    pas_de_place_error EXCEPTION;
BEGIN
    SELECT count(*) INTO nb_client_traj FROM avis WHERE id_trajet=:new.id_trajet;
    SELECT nbplaces INTO nb_places FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet WHERE id_trajet=:new.id_trajet GROUP BY nbplaces;
    IF nb_client_traj>=nb_places THEN
        RAISE pas_de_place_error;
    END IF;
    EXCEPTION
        WHEN pas_de_place_error THEN
        RAISE_APPLICATION_ERROR(-20002, 'Il ne peut pas y avoir plus d avis');
END;
/

--test trigger ci-dessus
---Création d'un client 
--insert into client values (5664, 'test', 'test');
---Vérification du nombre d'avis laissé sur une voiture
--select nbplaces FROM voiture NATURAL JOIN conducteur NATURAL JOIN trajet where id_trajet=1 group by nbplaces;

---Vérification du nombre de places dans la voiture
--select count(*) from avis where id_trajet=1;

---Modification du nombre de places de la voiture 1
--update voiture set nbplaces=2 where id_voiture=1;

---Création de l'avis (doit bloquer)
--insert into avis values ('tes', 5, 5664, 1);