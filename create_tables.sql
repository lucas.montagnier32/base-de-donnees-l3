
--set linesize 200;

prompt *************************************************************
prompt ********************* DROP INDEX *****************************
prompt *************************************************************

DROP INDEX idx_prix_brut;

prompt *************************************************************
prompt ******************** DROP TABLE *****************************
prompt *************************************************************

DROP TABLE TRAJET CASCADE CONSTRAINTS;
DROP TABLE CONDUCTEUR CASCADE CONSTRAINTS;
DROP TABLE CLIENT CASCADE CONSTRAINTS;
DROP TABLE AVIS CASCADE CONSTRAINTS;
DROP TABLE VOITURE CASCADE CONSTRAINTS;

prompt *************************************************************
prompt ******************** CREATE TABLE ***************************
prompt *************************************************************

-- Nous n'avons pas les privilÃ¨ges pour crÃ©er un cluster
--CREATE CLUSTER avis_trajet (id_trajet NUMBER)
    --SIZE 600
    --TABLESPACE users
    --STORAGE (INITIAL 100K NEXT 50K);
    	--CLUSTER avis_trajet(id_trajet);
   	--CLUSTER avis_trajet(id_trajet);

CREATE TABLE VOITURE (
	id_voiture NUMBER,
	modele VARCHAR2(40),
	couleur VARCHAR(20),
	nbplaces NUMBER,
	CONSTRAINT pk_voiture PRIMARY KEY(id_voiture),
	CONSTRAINT check_nbPlaces CHECK (nbplaces BETWEEN 1 AND 7)
);

CREATE TABLE CONDUCTEUR (
	id_cond NUMBER,
	id_voiture NUMBER,
	nom_cond	VARCHAR2(40),
	prenom_cond	VARCHAR2(40),
	CONSTRAINT pk_conducteur PRIMARY KEY(id_cond),
	CONSTRAINT fk_conducteur_voiture FOREIGN KEY(id_voiture) REFERENCES VOITURE(id_voiture)
);

CREATE TABLE TRAJET (
	id_trajet NUMBER,
	id_cond NUMBER,
	lieu_dep	VARCHAR2(40),
	lieu_arr	VARCHAR2(40),
	date_dep	TIMESTAMP,
	date_arr	TIMESTAMP,
	prix_brut NUMBER,
	marge NUMBER,
	CONSTRAINT pk_trajet PRIMARY KEY(id_trajet),
	CONSTRAINT fk_trajet_conducteur FOREIGN KEY(id_cond) REFERENCES CONDUCTEUR(id_cond),
	CONSTRAINT check_date_dep_arr CHECK (date_dep < date_arr),
	CONSTRAINT check_prix_marge CHECK (marge < prix_brut)
);

CREATE TABLE CLIENT (
	id_cli NUMBER,
	nom_cli VARCHAR2(40),
	prenom_cli VARCHAR2(40),
	CONSTRAINT pk_client PRIMARY KEY(id_cli)
);

CREATE TABLE AVIS (
	avis VARCHAR2(100),
	note NUMBER,
	id_cli NUMBER,
	id_trajet NUMBER,
	CONSTRAINT pk_avis PRIMARY KEY(id_trajet,id_cli),
	CONSTRAINT fk_trajet_avis_idci FOREIGN KEY(id_cli) REFERENCES CLIENT(id_cli),
	CONSTRAINT fk_trajet_avis_idtraj FOREIGN KEY(id_trajet) REFERENCES TRAJET(id_trajet),
	CONSTRAINT check_note CHECK (note BETWEEN 0 AND 5)
);

prompt *************************************************************
prompt ********************* DROP ROLE *****************************
prompt *************************************************************

DROP ROLE L3_35_37_client;
DROP ROLE L3_32_37_conducteur;
DROP ROLE L3_44_admin;

prompt *************************************************************
prompt ******************** CREATE ROLE ****************************
prompt *************************************************************

CREATE ROLE L3_35_37_client;
CREATE ROLE L3_32_37_conducteur;
CREATE ROLE L3_44_admin;

GRANT L3_44_admin TO L3_44;
GRANT L3_35_37_client, L3_32_37_conducteur TO L3_37;
GRANT L3_35_37_client TO L3_35;
GRANT L3_32_37_conducteur TO L3_32;

GRANT ALL ON CONDUCTEUR TO L3_44_admin;
GRANT ALL ON TRAJET TO L3_44_admin;
GRANT ALL ON CLIENT TO L3_44_admin;
GRANT ALL ON AVIS TO L3_44_admin;
GRANT ALL ON VOITURE TO L3_44_admin;

GRANT select, update, insert, delete on CLIENT TO L3_35_37_client;
GRANT select, update, insert, delete on AVIS TO L3_35_37_client;
GRANT select on CONDUCTEUR TO L3_35_37_client;
GRANT select on VOITURE TO L3_35_37_client;
GRANT select on TRAJET TO L3_35_37_client;

GRANT select, update, insert, delete on TRAJET TO L3_32_37_conducteur;
GRANT select, update, insert, delete on CONDUCTEUR TO L3_32_37_conducteur;
GRANT select, update, insert on VOITURE TO L3_32_37_conducteur;
GRANT select on AVIS TO L3_32_37_conducteur;
GRANT select on CLIENT TO L3_32_37_conducteur;


prompt *************************************************************
prompt ******************** CREATE INDEX ****************************
prompt *************************************************************

CREATE INDEX idx_prix_brut ON trajet (prix_brut);


prompt *************************************************************
prompt ********************* DROP VIEW *****************************
prompt *************************************************************

DROP VIEW trajet_une_personne;
DROP VIEW trajet_par_conducteur;

prompt *************************************************************
prompt ******************** CREATE VIEW ****************************
prompt *************************************************************

CREATE OR REPLACE VIEW trajet_une_personne AS
    SELECT *
    FROM trajet NATURAL JOIN avis;
    
CREATE OR REPLACE VIEW trajet_par_conducteur AS
    SELECT *
    FROM trajet NATURAL JOIN conducteur;
