
prompt *************************************************************
prompt ******************* DELETE TABLE ****************************
prompt *************************************************************

DELETE FROM AVIS;
DELETE FROM TRAJET;
DELETE FROM CONDUCTEUR;
DELETE FROM CLIENT;
DELETE FROM VOITURE;

prompt *************************************************************
prompt ********************** INSERT *******************************
prompt *************************************************************

DROP sequence seq_novoiture;
CREATE SEQUENCE seq_novoiture START WITH 50 INCREMENT BY 1;


insert into VOITURE values (1, 'Peugeot', 'Marron', 4);
insert into VOITURE values (2, 'Toyota', 'Grise', 4);
insert into VOITURE values (3, 'Twingo', 'Verte', 3);
insert into VOITURE values (4, 'Ferrari', 'Rouge', 3);
insert into VOITURE values (5, 'Peugeot', 'Marron', 3);
insert into VOITURE values (6, 'Renault', 'Blanche', 4);
insert into VOITURE values (7, 'Citroen', 'Blanche', 7);

insert into CONDUCTEUR values (1, 1, 'Tanguay','Arnaud');
insert into CONDUCTEUR values (2, 2, 'Fondemie','Chappell');
insert into CONDUCTEUR values (4, 3, 'Barrette','Ancelina');
insert into CONDUCTEUR values (6, 4, 'Neufville','Arlette');
insert into CONDUCTEUR values (8, 5, 'Barrette','Jacqueline');
insert into CONDUCTEUR values (10, 6, 'Neufville','Arlette');
insert into CONDUCTEUR values (13, 7, 'Faemlin','Ancelina');

insert into TRAJET values (1, 1, 'Nantes', 'Paris', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 31, 6);
insert into TRAJET values (2, 4, 'Nantes', 'Paris',TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 32, 6);
insert into TRAJET values (3, 2, 'Mulhouse', 'Marseille', TIMESTAMP '2015-03-12 8:37:00', TIMESTAMP '2015-03-12 17:00:00', 50, 10);
insert into TRAJET values (4, 6, 'Latronche', 'Arnac-La-Poste', TIMESTAMP '2018-04-14 05:08:00', TIMESTAMP '2018-04-14 07:30:00', 14, 2);
insert into TRAJET values (5, 8, 'Nantes', 'Tours', TIMESTAMP '2018-04-14 05:08:00', TIMESTAMP '2018-04-14 07:30:00', 14, 2);
insert into TRAJET values (6, 10, 'Arnac-La-Poste', 'Latronche', TIMESTAMP '2020-11-21 08:00:00', TIMESTAMP '2020-11-21 10:30:00', 15, 2);
insert into TRAJET values (7, 2, 'Mouais', 'Latronche', TIMESTAMP '2018-04-04 07:30:00', TIMESTAMP '2018-04-04 13:30:00', 15, 2);
insert into TRAJET values (8, 13, 'Poil', 'Plurien', TIMESTAMP '2002-08-07 13:00:00', TIMESTAMP '2002-08-07 18:00:00', 20, 4);

insert into CLIENT values (1, 'Tanguay','Arnaud');
insert into CLIENT values (2, 'Jacob','Ancelina');
insert into CLIENT values (3, 'Bordeaux','Victoire');
insert into CLIENT values (5, 'Girard','Astrid');
insert into CLIENT values (7, 'Bordeaux','Victoire');
insert into CLIENT values (9, 'Soucy','Iven');
insert into CLIENT values (11, 'Brunault','Astrid');
insert into CLIENT values (12, 'Brunault','Ruby');
insert into CLIENT values (14, 'Gamelin','Ruby');

insert into AVIS values ('Arnaud, il est bien!', 4, 2, 1);
insert into AVIS values ('Trajet satisfaisant', 3, 3, 1);
insert into AVIS values (':(', 1, 5, 2);
insert into AVIS values ('J ai bien aime ce trajet avec Angelina', 3, 1, 3);
insert into AVIS values ('Comentaire!!!', 1, 3, 4);
insert into AVIS values ('J adore la voiture', 4, 7, 4);
insert into AVIS values ('C est ici qu il faut mettre un commentaire?', 0, 9, 5);
insert into AVIS values ('Trajet tres agreable en compagnie d Arlette', 4, 11, 6);
insert into AVIS values ('5/5', 4, 12, 7);
insert into AVIS values ('Trajet satisfaisant', 3, 14, 8);



